data "aws_iam_account_alias" "current" {}

data "aws_caller_identity" "current" {}

data "aws_subnets" "subnets" {
  filter {
    name   = "vpc-id"
    values = [var.vpc_id]
  }

  filter {
    name   = "availability-zone"
    values = var.azs
  }

  tags = var.vpc_subnet_tags
}

data "aws_subnet" "subnets" {
  for_each = toset(data.aws_subnets.subnets.ids)

  id = each.value
}

data "aws_ami" "ami" {
  most_recent = true

  filter {
    name   = "name"
    values = [var.ami_search_string]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = [var.ami_account_id]
}