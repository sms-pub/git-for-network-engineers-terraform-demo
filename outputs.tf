output "instance_ids" {
  value = [for k in module.ec2-instance : k.id]
}

output "public_ips" {
  value = [for k in module.ec2-instance : k.public_ip]
}