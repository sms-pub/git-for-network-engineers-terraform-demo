variable "aws_region" {
  description = "AWS region"
  type        = string
  default     = "us-east-1"
}

variable "account_id" {
  description = "Account ID to deploy in"
  type        = string
}

variable "ami_account_id" {
  description = "Account ID where the AMIs are created"
  type        = string
}

variable "ami_search_string" {
  description = ""
  type        = string
  default     = "Cisco-C8K-17.07.01a-42cb6e93-8d9d-490b-a73c-e3e56077ffd1" //Cisco Catalyst 8000V Edge Software - BYOL
}

variable "vpc_id" {
  description = "VPC ID to deploy in"
  type        = string
}

variable "azs" {
  description = "AZs in us-gov-east"
  type        = list(string)
  default     = ["us-east-1a", "us-east-1b", "us-east-1c", ]
}

variable "vpc_subnet_tags" {
  description = "Map of tags used to query for subnets"
  type        = map(string)
  default = {
    Network = "Public" # Options - public, private, database
  }
}

variable "allowed_subnets" {
  description = "Subnets allowed to SSH to instances"
  type        = list(string)
  default = [
    "172.31.0.0/16",
  ]
}

variable "name" {
  description = "Default resource name"
  type        = string
  default     = "netdev"
}

variable "instances" {
  description = "Defines a list of instances and their names"
  type        = list(any)
  default = [
    "netdev-01",
  ]
}

variable "key_name" {
  description = "Key name of the Key Pair to use for the instance"
  type        = string
}

variable "public_key_hash" {
  description = "SSH Public Key Hash"
  type        = string
  default     = "EC0907F8069C51745FC515C10A1C7B47"
}

variable "default_tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default = {
    org      = "sms"
    team     = "tlp"
    tf-owned = "true"
  }
}

variable "dynamic_tags" {
  description = "A map of dynamic tags updated by CI/CD"
  type        = map(string)
  default = {
    env    = "env"
    repo   = "repo"
    branch = "branch"
  }
}