provider "aws" {
  # The default account where IAM user is created
  region = var.aws_region

  # Define default tags
  default_tags {
    tags = merge(var.default_tags, var.dynamic_tags)
  }
  # AWS Role to assume
  assume_role {
    role_arn    = "arn:aws:iam::${var.account_id}:role/TerraformAutomation"
    external_id = "terraform_only"
  }
}

locals {
  current_caller = regex("^.*:.*?/[[:alnum:]-]*", data.aws_caller_identity.current.arn)
  //  subnet_ids     = sort(tolist(data.aws_subnets.subnets.ids))
  hosts_subnets = { for i, v in var.instances : v => element(data.aws_subnets.subnets.ids, i) }
}

# Create Security Group for EC2 instance
resource "aws_security_group" "allow_ssh" {
  name        = "allow-ssh-sg"
  description = "Template Instance Security Group"
  vpc_id      = var.vpc_id

  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    self      = true
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = var.allowed_subnets
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    //    cidr_blocks = ["0.0.0.0/0"] #tfsec:ignore:AWS009
  }

  tags = merge(
    {
      Name  = "allow-ssh-sg"
      acct  = data.aws_iam_account_alias.current.account_alias
      owner = local.current_caller
    },
  )
}

module "ec2-instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "3.4.0"

  for_each = local.hosts_subnets

  name = each.key

  ami                         = data.aws_ami.ami.id
  instance_type               = "c5.large" // c5.large recommended
  key_name                    = var.key_name
  associate_public_ip_address = true
  subnet_id                   = each.value
  vpc_security_group_ids      = [aws_security_group.allow_ssh.id, ]

  //  user_data = templatefile("${path.module}/configs/user_data.tftpl", { #TODO
  //    hostname = each.key,
  //    public_key = var.public_key_hash,
  //  })

  metadata_options = {
    http_tokens = "required"
  }

  tags = merge(
    {
      acct  = data.aws_iam_account_alias.current.account_alias
      owner = local.current_caller
    },
  )
}